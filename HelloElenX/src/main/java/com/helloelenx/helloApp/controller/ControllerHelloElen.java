/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloelenx.helloApp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Wojciech
 */

@Controller
@RequestMapping(value = {"/index"})
public class ControllerHelloElen {
    
    private static final Log log = LogFactory.getLog(ControllerHelloElen.class);
    
    @GetMapping
    public String helloElenXPage() {
        return "helloElenx";
    }
    
}
